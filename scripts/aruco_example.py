#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
aruco_example.py: Get the video feed from the Vikking Drone and detect all Aruco
                  markers in the image.

"""

###############################################
# Standard Imports                            #
###############################################
import sys, time
import numpy as np

from scipy.ndimage import filters

###############################################
# OpenCV imports                              #
###############################################
import cv2

###############################################
# ROS Imports                                 #
###############################################
import roslib
import rospy

###############################################
# ROS Topic messages                          #
###############################################
from sensor_msgs.msg import CompressedImage

###############################################
# ROS Service messages                        #
###############################################


###############################################
# Aruco detector class                        #
###############################################
class aruco_detector:

    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        # topic where we publish
        self.image_pub = rospy.Publisher("/aruco/detected/image_raw/compressed",CompressedImage, queue_size = 1)
        # self.bridge = CvBridge()

        # subscribed Topic
        self.subscriber = rospy.Subscriber("/mono_cam_downward/image_raw/compressed",CompressedImage, self.cb_image, queue_size = 1)


    """
    Callbacks
    * cb_state
    """
    def cb_image(self, ros_data):
        '''Callback function for the downward onboard camera'''

        #### direct conversion to CV2 ####
        np_arr = np.frombuffer(ros_data.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        #image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:

        dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_250)
        parameters =  cv2.aruco.DetectorParameters_create()

        markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(image_np, dictionary, parameters=parameters)

        frame_markers = cv2.aruco.drawDetectedMarkers(image_np.copy(), markerCorners, markerIds)

        #### Create CompressedIamge ####
        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv2.imencode('.jpg', frame_markers)[1]).tobytes()

        # Publish new image
        self.image_pub.publish(msg)


def main(args):
    '''Initializes and cleanup ros node'''
    ad = aruco_detector()

    rospy.init_node('aruco_detector', anonymous=True)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down ROS Image feature detector module")

    #cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
