# Viking Drone Gazebo

# Prerequisite installations and configurations

## Robot Operating System (ROS)
Follow the guide from [the official ROS](http://wiki.ros.org/Distributions) webpage (depending on your Linux/Ubuntu distribution, tested on Ubuntu 20.04).

## PX4 Firmware

### Installation
Clone the PX4 firmware (v1.11.3) from github

```sh
cd ~
git clone https://github.com/PX4/PX4-Autopilot
cd ~/PX4-Autopilot
git checkout v1.11.3
git submodule update --init --recursive
```

Build the PX4 SITL firmware and run the Gazebo environment
```sh
cd ~/PX4-Autopilot
DONT_RUN=1 make px4_sitl_default gazebo
```

Install dependencies for PX4 SITL
```sh
cd ~/PX4-Autopilot_old/Tools/setup/
./ubuntu.sh
```

# Viking Drone development environment

![Screenshot](images/VikingDrone.png)

## Installation
Install following [MAVROS](http://wiki.ros.org/mavros) packages
```sh
sudo apt install ros-melodic-mavros ros-melodic-mavros-extras -y
```

and, install GeographicLib datasets by running the `install_geographiclib_datasets.sh` script:

```sh
cd ~
wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh
chmod 755 install_geographiclib_datasets.sh
sudo ./install_geographiclib_datasets.sh
```

## Create your ROS worlspace

#### Prerequisite installations
Install the [Catkin Command Line Tools](https://catkin-tools.readthedocs.io/en/latest/) package
```sh
pip3 install catkin-tools
```

#### Create your Viking Drone workspace
```sh
cd ~
mkdir -p vd_ws/src
cd ~/vd_ws/src
catkin_init_workspace
cd ..
catkin build
```

Clone and build the Viking Drone Gazebo repo

```sh
cd ~/vd_ws/src
git clone git@gitlab.com:viking-drone/vd_gazebo.git
catkin build
```

### Launching the PX4 SITL with ROS wrapper and MAVROS
Add the commands to your `.bashrc`
```sh
source /home/$USER/PX4-Autopilot/Tools/setup_gazebo.bash /home/$USER/PX4-Autopilot /home/$USER/PX4-Autopilot/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/PX4-Autopilot
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/PX4-Autopilot/Tools/sitl_gazebo
```
(and source the `.bashrc`)

Source the Viking Drone workspace

```sh
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/$USER/vd_ws/src/vd_gazebo/models
```

or, simply by running

```sh
source ~/vd_ws/src/vd_gazebo/setup_gazebo.bash
```

### Symlink the Viking Drone files into your PX4 SITL environment
Symlink the Viking Drone airframes, models and mixer files into your the 'PX4-Autopilot' folder,

```sh
ln -s /home/$USER/vd_ws/src/vd_gazebo/init.d-posix/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/
ln -s /home/$USER/vd_ws/src/vd_gazebo/mixers/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/mixers/
ln -s /home/$USER/vd_ws/src/vd_gazebo/models/* /home/$USER/PX4-Autopilot/Tools/sitl_gazebo/models/
```

or, simply by running

```sh
source ~/vd_ws/src/vd_gazebo/setup_posix.bash
```


### (Optional) Configure the Global coordinates
Set the Global coordinates by setting the following parameters
```sh
export PX4_HOME_LAT=55.4719762
export PX4_HOME_LON=10.3248095
export PX4_HOME_ALT=7.4000000
```
(Optional) Add the commands to your `.bashrc`.

### (Optional) Setup alias for your 

Add the commands to your `.bashrc`.
```sh
alias svd-ws="source ~/vd_ws/devel/setup.bash && source ~/vd_ws/src/vd_gazebo/setup_gazebo.bash"
alias pvd-ws="cd ~/vd_ws/ && svd-ws"
alias bvd-ws="pvd-ws && catkin build"
```
where 
* `svd-ws` is a shortcut for sourcing the Viking Drone workspace and the PX4-Autopilot firmware
* `pvd-ws` is a shortcut for sourcing both projects and go to the Viking Drone workspace
* `bvd-ws` is a shortcut for building, sourcing both projects and go to the Viking Drone workspace

## Running the development environment

### Single drone
Build and source the `vd_gazebo` package, eg. using the defined alias
```sh
bvd-ws
```
Start the PX4 SITL simulation environment
```sh
roslaunch vd_gazebo posix.launch vehicle:=<drone> env:=<world> mavros:=<activated?> x:=0 y:=0 z:=0.1 R:=0 P:=0 Y:=0
```
where
* `vehicle`, defines the which drone model to spawn. The available models are:
  * `viking_drone`, **default version**, indluding a Stereo Camera (facing forward) and a Mono Camera (facing downward)
  * `viking_drone_depth`, Viking Drone (default version) including a **Depth Camera (facing forward)**
  * `viking_drone_lidar`, Viking Drone (default version) including a **Lidar (facing forward)**
  * `viking_drone_sonar`, Viking Drone (default version) indluding a **Sonar (facing forward)**
  * `viking_drone_sonar_360`, Viking Drone (default version) indluding **four Sonars** (facing forward, backward, left and right)
  * `viking_drone_gimbal`, Viking Drone (default version) indluding a **Gimbal (with a Mono Camera)** controlled using MAVLink messages
* `env`, defines the which Gazebo world to open. The available Gazebo worlds are:
  * `empty`, default world
  * `airfield`
  * `aruco`
  * `outdoor_village`
  * `raceway`
  * `random_obstacle`
  * `simple_obstacle`
  * `warehouse`
  * `wind_turbine`
  * `windy`
* `mavros`, either `true` or `false` (default) depending on if MAVROS should be included or not. 
* `gui`, either `true` (default) or `false` (also known as HEADLESS mode) depending on if you are interested in running the graphical interface  
* `x`, `y` and `z` are used for setting where the drone should be spanwed (in the local coordinate system).  
* `R`, `P` and `Y`, are used for setting the attitude of the drpne, when it is being spawned.

### Multiple drones 
The multiple drone setup consists of one `roslaunch` running the Gazebo world and multiple `roslaunch`s, one for each drone you like to spawn into the Gazebo world. 

#### Prerequisite installations
Ensuer that you have xmlstarlet installed
```sh
sudo apt install xmlstarlet
``` 

#### Start your Gazebo world
Start the Gazebo world
```sh
roslaunch vd_gazebo posix_world.launch env:=<world>
```

#### Spawn Viking Drones into your Gazebo world
Open a new terminal, and 
```sh
roslaunch vd_gazebo posix_spawn.launch vehicle:=<drone> ID:=<ID_numver> mavros:=<activated?> x:=0 y:=0 z:=0.1 R:=0 P:=0 Y:=0
```
where, 
* `ID`, is defining the **unique number** for the spawned drone. **Remember** to change the `ID` when spawning multiple drones. 
