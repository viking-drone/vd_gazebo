# px4-simulation-docker

Docker files for the PX4 STIL Simulation container

# Building the container
```sh
docker-compose build
```

# RUN
```sh
docker-compose up
```

# Check containers
```sh
docker ps
```
