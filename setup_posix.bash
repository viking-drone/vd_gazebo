#!/bin/bash

echo "Cleaning up"
find ~/PX4-Autopilot/ -name "*viking*" -delete

echo "Symlink"
#ln -s /home/$USER/vd_workspace/src/vd_gazebo/init.d-posix/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/airframes/
ln -s /home/$USER/vd_ws/src/vd_gazebo/init.d-posix/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/
ln -s /home/$USER/vd_ws/src/vd_gazebo/mixers/* /home/$USER/PX4-Autopilot/ROMFS/px4fmu_common/mixers/
ln -s /home/$USER/vd_ws/src/vd_gazebo/models/* /home/$USER/PX4-Autopilot/Tools/sitl_gazebo/models/
